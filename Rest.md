# **What is REST?**

REST stands for **RE**presentational **S**tate **T**ransfer. It was defined by *Roy Fielding*, a computer scientist in 2000. It is a design pattern for APIs.
**API** stands for **A**pplication **P**rogram **I**nterface.

It is a design pattern for providing set of protocols to be used of creating web services which allows systems to communicate with each other on web.
```
"In other words, if you want to interact with your system to retrieve information, an API helps you to communicate what you want to that system so it can understand and fulfill your request." 
```
A REST based system consists of following key terms:

* **client** who requests for the resources.
* **server** who has resources.


When a RESTful API is called, *the server will transfer to client a representation of the state of the requested resource*.

For example, when a developer calls Instagram API to fetch a specific user (the resource), the API will return the state of that user, including their name, the number of posts that user posted on Instagram so far, how many followers they have, and more.

The representation of the state can be in a JSON format or it can be in XML or HTML format.

What the server does when a user, the client, call one of its APIs depends on two things you need to provide to the server:

* An identifier for the resource you are interested in i.e., URL for the resource which is also known as **endpoint**.
* The operation you want the server to perform on that resource, in the form of **HTTP method**, The common methods are GET, POST, PUT and DELETE.

<img src="https://phpenthusiast.com/theme/assets/images/blog/what_is_rest_api.png" width="800" height="auto" margin-left="10px">

---
# **REST Architectural Constraints**

REST has **6 architectural constraints** which helps to make web service or a RESTful API.

1. Uniform interface
1. Client-server
1. Stateless
1. Cacheable
1. Layered system
1. Code on demand

## Uniform interface

* It the key differentiator between **REST** and **Non REST APIs**.
* There are four elements of Uniform Resource Constraint as follow:
  * Identification of resources which means that, The request to the server has to include resource identifier.
  * Manipulation of resources through representation.
  * Self descriptive message for each request which means that each request contains all the information the server needs to perform the request and each response contains all the information the client needs in order to understand the response.
  * **HATEOS** (Hypermedia As The Engine Of Application State).

## Client-server

* This constraint states that REST application should have client-server architecture.
* Advantage is client and server are separate.
* They can evolve independently.
* Clients need not know about business logic / data access layer.
* Servers need not know anything about the frontend UI. 

## Stateless

Stateless constraint states that the server does not store any session data means that, Server does not store any kind of data related to client whereas, Client is free to store session information in their context.

## Cacheable

* Cache constraints states that responses should be cacheable, if possible.
* Caching helps to improve performance for client-side and better scope for a server because the load has reduced.

## Layered System

* Allows an architecture to be composed of hierarchial manner.
* Each layer doesn't know about anything beyond the intermediate layer.
* Due to this kind of system, the application has better security in each layer can't interact outside another layer.

## Code on Demand

This is an optional constraint. In addition to data, the servers can provide executable code to client.

# Conclusion

* REST is a type of data transfer that is built upon th architecture of the HTTP protocol. It allows you to easily send and retrieve data between two different services
When designing your web applications, it is a good practice to build them using RESTful architecture.
---
## Resources

1. [https://www.geeksforgeeks.org/rest-api-architectural-constraints/](https://www.geeksforgeeks.org/rest-api-architectural-constraints/)
1. [https://www.redhat.com/en/topics/api/what-is-a-rest-api](https://www.redhat.com/en/topics/api/what-is-a-rest-api)
1. [https://medium.com/extend/what-is-rest-a-simple-explanation-for-beginners-part-2-rest-constraints-129a4b69a582](https://medium.com/extend/what-is-rest-a-simple-explanation-for-beginners-part-2-rest-constraints-129a4b69a582)
1. [https://restfulapi.net/rest-architectural-constraints/](https://restfulapi.net/rest-architectural-constraints/)
1. [https://www.youtube.com/watch?v=Q-BpqyOT3a8&t=656s](https://www.youtube.com/watch?v=Q-BpqyOT3a8&t=656s)
